const { randomUserMock, additionalUsers } = require('./mock_for_L3');


/*TASK 1*/
function task_1(in_array, b_array) {
  const p_array = in_array.map((item) => ({
    gender: item.gender,
    title: item.name.title,
    full_name: (`${item.name.first} ${item.name.last}`),
    city: item.location.city,
    state: item.location.state,
    country: item.location.country,
    postcode: item.location.postcode,
    coordinates: item.location.coordinates,
    timezone: item.location.timezone,
    email: item.email,
    b_date: item.dob.date,
    age: item.dob.age,
    phone: item.phone,
    picture_large: item.picture.large,
    picture_thumbnail: item.picture.thumbnail,
  }));
  const k_array = p_array.map((item) => JSON.stringify(item));
  return p_array.concat(b_array.filter((item) => !k_array.includes(JSON.stringify(item))));
}
const result_task_1 = task_1(randomUserMock, additionalUsers);
console.log(result_task_1);


/*TASK 2*/
function email_val(email)
{
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
}
function phone_val(phone) {
  const phoneRe = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
  const digits = phone.replace(/\D/g, '');
  return phoneRe.test(digits);
}
function task_2(object) { 
  try {
    [object.full_name, object.gender, object.note, object.state, object.city, object.country].forEach((item) => {
      if (item[0] === item[0].toLowerCase()) throw new Error();
    });
    Number(object.age);
    if (!email_val(object.email)) throw new Error();
    if (!phone_val(object.phone)) throw new Error();
    return true;
  } catch (err) {
    return false;
  }
}
const result_task_2 = task_2(result_task_1[0]);
console.log(result_task_2);



/*TASK 3*/
function task_3(array, callback) {
  return array.filter((item) => callback(item.country, item.age, item.gender, item.favorite));
}
const result_task_3 = task_3(result_task_1.slice(1, 5), (country, age, gender, favorite) => {
  if (country !== 'Zambia') return false;
  if (gender !== 'Male') return false;
  if (typeof favorite !== null) return false;
  if (age > 30) return false;
  return true;
});
console.log(result_task_3);



/*TASK 4*/
function task_4(array, key) {
  return array.sort((item, item2) => {
    if (!item[key]) return 0;
    if (!item2[key]) return 0;
    if (typeof item[key] === 'string') {
      return item[key].localeCompare(item2[key]);
    }
    return item[key] - item2[key];
  });
}
const result_task_4 = task_4(result_task_1.slice(1,5), 'age')


/*TASK 5*/
function task_5(array, key, value) {
  try {
    return array.filter((item) => String(item[key]) === String(value))[0];
  } catch(err) {
  }
}
const result_task_5 = task_5(result_task_1, 'age', 65);
console.log(result_task_5);


/*TASK 6*/
function task_6(array, callback) {
  const f_array = array.filter((item) => callback(item));
  return ((f_array.length * 100) / array.length).toString() + ' %';
}
const result_task_6 = task_6(result_task_1, (item) => item.age ? item.age < 40 : true);
console.log(result_task_6);