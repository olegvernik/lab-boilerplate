function makeID() {
  let result = '';
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  for (let i = 0; i < 2; i++) result += chars.charAt(Math.floor(Math.random() * chars.length));
  result += Math.random().toString().substr(2, 11);
  return result;
}

function makeBGColor() {
  let result = '';
  const chars = 'abcdef0123456789';
  for (let i = 0; i < 6; i++) result += chars.charAt(Math.floor(Math.random() * chars.length));
  return result;
}

function capitalizeFirstLetter(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function getAge(bDay) {
  return parseInt((new Date(Date.now() - Date.parse(bDay))).getUTCFullYear() - 1970, 10);
}

function stringFieldComparator(a, b) {
  const fa = a.toLowerCase();
  const fb = b.toLowerCase();
  if (fa < fb) return 1;
  if (fa > fb) return -1;
  return 0;
}

module.exports = {
  makeID, makeBGColor, capitalizeFirstLetter, getAge, stringFieldComparator,
};
