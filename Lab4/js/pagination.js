export default {
  paginate(page, per_page, items, max_pages, default_pages = 8) {
    if (items.length === 0) return items;

    if (!page || !per_page) {
      items.splice(default_pages);
      return items;
    }

    if (per_page > max_pages) per_page = max_pages;
    const count = Math.ceil(items.length / per_page);

    if (page > count) {
      if (items.length < per_page) return [];
      return items.splice(items.length - per_page);
    }

    return items.splice((page - 1) * per_page, per_page);
  },
};
