const codes = require('./../codes.json')

function isUpperCase(str) {
    if (typeof str === 'string' || str instanceof String) 
        return str.charAt(0).toUpperCase() === str.charAt(0)
    return false
}

function validateUser(person) {
    if (!(isUpperCase(person.gender) && isUpperCase(person.full_name) && (person.state ? isUpperCase(person.state) : true) && isUpperCase(person.city) && isUpperCase(person.country)))
        return false 

    const phone = person.phone.replace(/[.,/\s#!$%^&* :{}=\-_`~()]/g, '') 
    const { country } = person 
    const codeInfo = codes.find(code => code['eng'] === country)
    
    if (!codeInfo) 
        return false 
    if (!(phone.startsWith(codeInfo.code.replace(0, 0, '')) && parseInt(phone.replace('+', '', '').length, 10) === parseInt(codeInfo.length, 10))) 
        return false 
    
    return /\S+@\S+\.\S+/.test(person.email) 
}


module.exports = {isUpperCase, validateUser}