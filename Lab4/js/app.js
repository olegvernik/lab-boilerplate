require('../css/app.css');
require('../scss/style.scss');

const { randomUserMock, additionalUsers } = require('./mock_for_L3');
const {
  formatMockUsers, compileUserObjects, sortUsersByField, searchUsersByField,
} = require('./user_functions');
const { validateUser } = require('./user_validation');
const { makeID } = require('./helper_functions');

const teachers = compileUserObjects(formatMockUsers(randomUserMock), additionalUsers);

const favs = document.querySelector('#carousel-fav-list ul');
favs.innerHTML = '';


/*Get Top Teacher (With HTML Code)*/

function getTopTeacher(id, name, fav, country, url, addStar = true) {
  let title;
  if (!url) {
    const fullname = name.split(' ');
    title = `<p>${fullname[0][0]}. ${fullname[1][0]}.</p>`;
  } else title = `<img src="${url}" alt="Teacher pic"></img>`;
  const star = addStar ? `<img class="star" src="./images/${fav ? 'filledstar.png' : 'emptystar.png'}" alt="teacher favorited">` : '';
  return `<li class="teacher" data-id="${id}">
               ${star}
               <div class="comp-card">
                  <a href="#info-card" onclick="disablebg()" class="top-avatar">
                      ${title} 
                  </a>
                  <p>${name}</p>
               </div>
               <p>${country}</p>
            </li>`;
}


/*Fill Popup (With HTML Code)*/ 

function fillPopup(teacher) {
  if (!teacher) return;

  const email = teacher.email ? teacher.email : '';
  const phone = teacher.phone ? teacher.phone : '';

  const card = document.getElementById('card');
  card.dataset.bgcolor = teacher.bg_color;
  card.innerHTML = `
        <img id="avatar" itemprop="image" src=${teacher.picture_large ? teacher.picture_large : './images/placeholderimg.jpg'} alt="teacher's picture">
            <section id="info-caption">
                <p itemprop="name">${teacher.full_name}</p>
                <img id="fav-star" src="./images/${teacher.favorite ? 'filledstar.png' : 'emptystar.png'}" alt="favorite">
            </section>
        <section id="personal-info">
            <p itemprop="nationality">${teacher.city}, ${teacher.country}</p>
            <p itemprop="gender">${teacher.age}, ${teacher.gender[0].toUpperCase()}</p>
            <a itemprop="email" href="mailto:${email}"><p>${email}</p></a>
            <a itemprop="contactPoint" href="tel:${phone}"><p>${phone}</p></a>
        </section>
        <section id="other">
            <p>${teacher.note ? teacher.note : 'No comments'}</p>
            <a>toggle map</a>
        </section>
    `;
  document.getElementsByClassName('popup')[1].style.display = 'block';
  if (window.screen.width < 768) document.getElementById('info-popup').style.height = '400px';
}


/*Update Favorities*/

function updateFavorites() {
  favs.innerHTML = '';
  teachers.filter((teacher) => teacher.favorite)
    .slice(0, 5)
    .forEach((fav) => {
      const item = document.createElement('li');
      item.innerHTML = getTopTeacher(
        fav.id,
        fav.full_name,
        fav.favorite,
        fav.country,
        fav.picture_large,
        false,
      );
      favs.appendChild(item);
    });
}


/*GetTopTeachers*/

function getTopTeachers(teachersArray) {
  let html = '';
  teachersArray.forEach((teacher) => {
    const item = getTopTeacher(
      teacher.id,
      teacher.full_name,
      teacher.favorite,
      teacher.country,
      teacher.picture_large,
    );
    html += item;
  });

  const list = document.querySelector('#top-teachers ul');
  list.innerHTML = html;
  return list;
}


/*ApplyFilters*/

function applyFilters(teachersArray) {
  let filtered = teachersArray;
  if (document.getElementById('photo-filter').checked) filtered = filtered.filter((p) => p.picture_large != null);

  if (document.getElementById('fav-filter').checked) filtered = filtered.filter((p) => p.favorite === true);

  const country = document.getElementById('country-filter').selectedOptions[0].text;
  if (country !== 'All') filtered = filtered.filter((p) => p.country === country);

  if (document.getElementById('age-check-filter').checked) filtered = filtered.filter((p) => p.age === parseInt(document.getElementById('age-filter').value, 10));

  if (document.getElementById('gender-check-filter').checked) filtered = filtered.filter((p) => p.gender === document.getElementById('gender-filter').value);

  return filtered;
}


/*Fill Statistics Table*/ 

function fillStatTable(teachersArray) {
  const table = document.querySelector('.stat-table tbody');
  table.innerHTML = '';
  teachersArray.forEach((teacher) => {
    const row = document.createElement('tr');
    const fields = ['full_name', 'age', 'gender', 'country'];
    fields.forEach((key) => {
      const child = document.createElement('td');
      child.innerText = teacher[key];
      row.appendChild(child);
    });
    table.appendChild(row);
  });
}


/*Get Elements*/

document.getElementsByClassName('filters')[0].addEventListener('change', () => {
  const filtered = applyFilters(teachers);
  getTopTeachers(filtered);
});

document.getElementsByClassName('stat-table')[0].addEventListener('click', (e) => {
  if (e.target.nodeName === 'TH') {
    const sorted = sortUsersByField(teachers, e.target.id.split('-')[1], 1);
    fillStatTable(sorted.slice(0, 8));
  }
});

const top = getTopTeachers(teachers);

top.addEventListener('click', (e) => {
  if (e.target.parentElement.className === 'top-avatar') fillPopup(teachers.find((p) => p.id === e.target.parentElement.parentElement.parentElement.dataset.id));

  if (e.target.className === 'star') {
    const teacher = teachers.find((p) => p.id === e.target.parentElement.dataset.id);
    teacher.favorite = !teacher.favorite;
    e.target.src = `./images/${teacher.favorite ? 'filledstar.png' : 'emptystar.png'}`;
    updateFavorites();
  }
});

document.getElementById('carousel-fav-list').addEventListener('click', (e) => {
  fillPopup(teachers.find((p) => p.id === e.target.parentElement.parentElement.dataset.id));
});

document.getElementById('searchbar').addEventListener('submit', (e) => {
  e.preventDefault();
  const query = e.target[0].value.split(':');
  let teacher;
  if (query[0] === 'note') teacher = teachers.find((person) => person.note && person.note.includes(query[1]));
  else {
    teacher = searchUsersByField(
      teachers,
      query[0],
      (user, field) => user[query[0]].includes(query[1]),
    );
  }

  if (teacher) {
    fillPopup(teacher);
    window.location.replace('/#info-card');
  } else window.alert('No teachers were found for this query.');
});

fillStatTable(teachers.slice(0, 8));

const form = document.querySelector('#add-popup form');

form.addEventListener('submit', (e) => {
  e.preventDefault();
  const newTeach = {
    id: makeID(),
    favorite: false,
    course: '',
    bg_color: document.getElementById('bgcolor-palette').value,
    gender: document.getElementById('male-add').value ? 'Male' : 'Female',
    title: null,
    full_name: document.getElementById('fname-add').value,
    city: document.getElementById('city-add').value,
    state: null,
    country: document.getElementById('country-add').options[document.getElementById('country-add').selectedIndex].text,
    postcode: null,
    coordinates: null,
    timezone: null,
    email: document.getElementById('email-add').value,
    b_date: null,
    age: null,
    phone: document.getElementById('phone-add').value,
    picture_large: null,
    picture_thumbnail: null,
    note: null,
  };
  if (validateUser(newTeach)) {
    teachers.push(newTeach);
    getTopTeachers(teachers);
  } else {
    alert('Invalid data when adding teacher.');
  }
});
