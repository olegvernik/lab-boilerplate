const helpers = require('./helper_functions');

function formatMockUsers(users) {
  return users.map((person) => {
    const re1 = /[A-Z]{2}/g;
    const re2 = /[0-9]{11}/g;
    return {
      gender: person.gender,
      title: person.name.title,
      full_name: `${person.name.first} ${person.name.last}`,
      city: person.location.city,
      state: person.location.state,
      country: person.location.country,
      postcode: person.location.postcode,
      coordinates: person.location.coordinates,
      timezone: person.location.timezone,
      email: person.email,
      b_date: person.dob.date,
      age: person.dob.age,
      phone: person.phone,
      picture_large: person.picture.large,
      picture_thumbnail: person.picture.thumbnail,
      id: re1.test(person.id.name) && re2.test(person.id.value)
        ? person.id.name.concat(person.id.value)
        : helpers.makeID(),
      favorite: false,
      course: '',
      bg_color: helpers.makeBGColor(),
      note: '',
    };
  });
}

function compileUserObjects(mock, add) {
  mock.push(...add);
  const uniqueUsers = [];
  mock.forEach((mockUser) => {
    if (!uniqueUsers.find((user) => mockUser.full_name === user.full_name)) {
      uniqueUsers.push(mockUser);
    }
  });
  return uniqueUsers;
}

function sortUsersByField(users, field, order) {
  if (!(users && field && order)) return [];
  if (typeof users[0][field] === 'number') return users.sort((user1, user2) => order * (user2[field] - user1[field]));
  if (typeof users[0][field] === 'string' || users[0][field] instanceof String) return users.sort((user1, user2) => order * helpers.stringFieldComparator(user2[field], user1[field]));
  return users.sort();
}

function searchUsersByField(users, field, parameter) {
  return users.find((user) => parameter(user, field));
}

function searchCorrespondencePercent(users, field, parameter) {
  let count = 0;
  const len = users.length;
  users.forEach((user) => {
    if (parameter(user, field)) count += 1;
  });
  return len ? (100 * count) / len : 0;
}

module.exports = {
  sortUsersByField,
  searchCorrespondencePercent,
  searchUsersByField,
  formatMockUsers,
  compileUserObjects,
};
